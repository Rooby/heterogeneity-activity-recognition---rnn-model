# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow.python.estimator.canned.dnn import DNNRegressor
from tensorflow.contrib.estimator.python.estimator.rnn import RNNClassifier
from tensorflow.contrib.feature_column.python.feature_column.sequence_feature_column import sequence_numeric_column


# Helper libraries
import numpy as np
from numpy import genfromtxt

def get_csv_data(filepath):
    data = genfromtxt(filepath, delimiter=',')

    index = []
    x = []
    y = []
    z = []
    movement = []

    i = 0
    is_first_line = True
    dictionary = {}

    for line in data:
        if is_first_line == False:
            index.append(line[0])
            x.append(float(line[1]))
            y.append(float(line[2]))
            z.append(float(line[3]))
            movement.append(int(line[4]))
        is_first_line = False

    features = {
        "index": index,
        "x": x,
        "y": y,
        "z": z
    }
    labels = movement

    return features, labels

def get_csv_sparse_tensor_data(filepath, type):
    data = genfromtxt(filepath, delimiter=',')
    is_first_line = True

    index = []
    x = []
    y = []
    z = []
    movement = []
    indices = []
    i = 0;
    counter = 0

    if type == "train":
        for line in data:
            if is_first_line == False:
                if i % 1000 == 0:
                    indices.append([counter])
                    index.append(line[0])
                    x.append(float(line[1]))
                    y.append(float(line[2]))
                    z.append(float(line[3]))
                    movement.append(int(line[4]))
                    counter = counter + 1
                i = i + 1

                if i > 10000:
                    break
            is_first_line = False
    else:
        if is_first_line == False:
            indices.append([counter])
            index.append(line[0])
            x.append(float(line[1]))
            y.append(float(line[2]))
            z.append(float(line[3]))
            movement.append(int(line[4]))
            counter = counter + 1
            i = i + 1

        is_first_line = False

    if type == "train":
        sparse_index = tf.SparseTensor(indices, index, [len(index)])
        sparse_x = tf.SparseTensor(indices, x, [len(x)])
        sparse_y = tf.SparseTensor(indices, y, [len(y)])
        sparse_z = tf.SparseTensor(indices, z, [len(z)])

        features = {
            "Index": sparse_index,
            "x": sparse_x,
            "y": sparse_y,
            "z": sparse_z
        }
    else:
        features = {
            "Index": counter,
            "x": x,
            "y": y,
            "z": z
        }

    return features, movement

def train_input_fn(features, labels, batch_size):
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices((dict(features), labels))

    # Shuffle, repeat, and batch the examples.
    dataset = dataset.batch(batch_size)

    #iterator = dataset.make_one_shot_iterator()
    #next_element = iterator.get_next()
    #print(next_element)

    # Return the dataset.
    return dataset

def train_rnn_input_fn(file, batch_size):
    features, labels = get_csv_sparse_tensor_data(file, "train")
    # Return the dataset.
    return features, labels

def predict_rnn_input_fn(file, batch_size):
    dataset = tf.contrib.data.make_csv_dataset(file, batch_size=12000, column_names=["Index", "x", "y", "z", "label"], label_name="label")
    #features, labels = get_csv_sparse_tensor_data(file, "predict")
    #dataset = tf.data.Dataset.from_tensor_slices((features, labels))
    # Return the dataset.
    return dataset

def create_model(features, labels, mode, params):
    net = tf.feature_column.input_layer(features, params['feature_columns'])

    for units in params['hidden_units']:
        net = tf.layers.dense(net, units=units, activation=tf.nn.relu)

    logits = tf.layers.dense(net, params['n_classes'], activation=None)

    estimator_spec = tf.estimator.EstimatorSpec(
      mode=mode,
      predictions=predictions_dict,
      loss=loss,
      train_op=train_op,
      eval_metric_ops=eval_metric_ops,
      export_outputs=export_outputs)

    return estimator_spec

def create_dnn_regressor_estimator(output_dir):
    feature_columns = [
        tf.feature_column.numeric_column("index"),
        tf.feature_column.numeric_column("x"),
        tf.feature_column.numeric_column("y"),
        tf.feature_column.numeric_column("z")
    ]

    # Custom estimator attempt
    #estimator = tf.estimator.Estimator(
    #    model_fn=create_model,
    #    params={
    #        'feature_columns': feature_columns,
    #        'hidden_units': [10, 10],
    #        'n_classes': 7,
    #    }
    #)

    estimator = DNNRegressor(
        feature_columns=feature_columns,
        hidden_units=[10, 10, 10, 10],
        model_dir=output_dir
    )

    return estimator

def create_rnn_classifier_estimator(output_dir):
    feature_columns = [
        sequence_numeric_column(key="Index"),
        sequence_numeric_column(key="x"),
        sequence_numeric_column(key="y"),
        sequence_numeric_column(key="z"),
    ]

    context_columns = [
        tf.feature_column.numeric_column(key="x"),
        tf.feature_column.numeric_column(key="y"),
        tf.feature_column.numeric_column(key="z")
    ]

    estimator = RNNClassifier(
        sequence_feature_columns=feature_columns,
        cell_type='lstm',
        num_units=[10, 10, 10, 10],
        rnn_cell_fn=None,
        n_classes=7
    )

    return estimator