# TensorFlow and tf.keras
from numpy import genfromtxt
import matplotlib.pyplot as plt

import csv

file = "./datasets/Phones_accelerometer2.csv"

data = genfromtxt(file, delimiter=',')

# 1 - "Sit"
# 2 - "Stand"
# 3 - "Walk"
# 4 - "Stairsup"
# 5 - "Stairsdown"
# 6 - "Bike"

index = []
x = []
y = []
z = []
movement = []
i = 0
counter = 0

for line in data:
    if counter != 0:
        if i % 1000 == 0:
            index.append(i)
            x.append(float(line[1]))
            y.append(float(line[2]))
            z.append(float(line[3]))
            movement.append(int(line[4]))
    i = i + 1
    counter = 1

plt.plot(index, x, 'b', label='X')
plt.plot(index, y, 'g', label='Y')
plt.plot(index, z, 'r', label='Z')
plt.plot(index, movement, 'y', label='Movement')
plt.title("Timelapse")
plt.xlabel("Index")
plt.ylabel("Values")
plt.legend()
plt.show()