import helpers

batch_size = 1000

est = helpers.create_rnn_classifier_estimator("./outputdir")

phones_csv = "./datasets/Phones_accelerometer2.csv"

test = est.train(
    steps=1000,
    input_fn=lambda : helpers.train_rnn_input_fn(phones_csv, batch_size)
)
print("trained")
watch_csv = "./datasets/Watch_accelerometer2.csv"

metrics = est.evaluate(input_fn=lambda : helpers.train_rnn_input_fn(watch_csv, batch_size), steps=10)
print(metrics)
prediction_csv = "./datasets/Watch_accelerometer2_prediction.csv"
prediction = est.predict(
    input_fn=lambda : helpers.train_rnn_input_fn(prediction_csv, batch_size)
)
prediction_values = list(prediction)
print(prediction_values)


